# necromancer
> I wish it need not have happened in my time," said Frodo. "So do I," said Gandalf, "and so do all who live to see such times. But that is not for them to decide. All we have to decide is what to do with the time that is given us. - J. R. R. Tolkien.

Necromancer is a 4 wheel vehicle build around a raspberry Pi  capable of recognize specific objects and follow them around with the help of a webcam and tensorflow.

This project has 3 main modules, a Motor Driver, an Object Dection System and the Follower itself.

## Motor Driver
The idea is to have a realible, fast and easy to use way to control 4 motors whit a LM293D as H-bridge.

This module was develop in C whit the [bcm2835 library](http://www.airspayce.com/mikem/bcm2835/index.html) by Mike McCauley. 

There are 6 self explanatory functions avalible as independent binaries:

1. end()
2. front()
3. back()
4. left()
5. back()
6. stop()

After everything is complete all the pins need to be set to the default states, end() takes care of that.

## Object Detection System
ToDo

## Follower
ToDo


