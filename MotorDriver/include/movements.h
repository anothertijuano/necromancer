#include "gpio.h"
#include "pinDef.h"

int front();
int back();
int stop();
int left();
int right();

int front()
{
     writePin(E1, LOW);
     writePin(E2, LOW);
     writePin(INPUT1, LOW);
     writePin(INPUT2, HIGH);
     writePin(INPUT3, LOW);
     writePin(INPUT4, HIGH);
     writePin(E1, HIGH);
     writePin(E2, HIGH);
}

int back()
{
     writePin(E1, LOW);
     writePin(E2, LOW);
     writePin(INPUT1, HIGH);
     writePin(INPUT2, LOW);
     writePin(INPUT3, HIGH);
     writePin(INPUT4, LOW);
     writePin(E1, HIGH);
     writePin(E2, HIGH);
}

int stop()
{
     writePin(E1, LOW);
     writePin(E2, LOW);
     writePin(INPUT1, LOW);
     writePin(INPUT2, LOW);
     writePin(INPUT3, LOW);
     writePin(INPUT4, LOW);
}

int left()
{
     writePin(E1, LOW);
     writePin(E2, LOW);
     writePin(INPUT1, LOW);
     writePin(INPUT2, HIGH);
     writePin(INPUT3, HIGH);
     writePin(INPUT4, LOW);
     writePin(E1, HIGH);
     writePin(E2, HIGH);
}

int right()
{
     writePin(E1, LOW);
     writePin(E2, LOW);
     writePin(INPUT1, HIGH);
     writePin(INPUT2, LOW);
     writePin(INPUT3, LOW);
     writePin(INPUT4, HIGH);
     writePin(E1, HIGH);
     writePin(E2, HIGH);
}


