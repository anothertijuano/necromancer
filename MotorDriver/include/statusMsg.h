#include<stdio.h>


int msgOk2(char* msg) 
{
     printf("\033[1;32m");
     printf("%s",msg);
     printf("\033[0m");
}

int msgOk(char* msg) 
{
     printf("\033[0;32m");
     printf("%s",msg);
     printf("\033[0m");
}

int msgError(char* msg) 
{
     printf("\033[1;31m");
     printf("%s",msg);
     printf("\033[0m");
}

int msgWarning(char* msg)
{
     printf("\033[1;33m");
     printf("%s", msg);
     printf("\033[0m");
}


