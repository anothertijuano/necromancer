#include <bcm2835.h>
#include <time.h>
#include "pinDef.h"
#include <stdlib.h>

int init();
uint8_t readPin(int PIN);
int writePin(int PIN, uint8_t BOOL);
int blink(int PIN, uint64_t delay);
int end();

int init()
{
     if (!bcm2835_init())
          return 1;

     // Set the pin to be an output
     bcm2835_gpio_fsel(INPUT1, BCM2835_GPIO_FSEL_OUTP);
     bcm2835_gpio_fsel(INPUT2, BCM2835_GPIO_FSEL_OUTP);
     bcm2835_gpio_fsel(INPUT3, BCM2835_GPIO_FSEL_OUTP);
     bcm2835_gpio_fsel(INPUT4, BCM2835_GPIO_FSEL_OUTP);
     bcm2835_gpio_fsel(E1, BCM2835_GPIO_FSEL_OUTP);
     bcm2835_gpio_fsel(E2, BCM2835_GPIO_FSEL_OUTP);
     return(0);
}

uint8_t readPin(int PIN)
{
     return(bcm2835_gpio_lev(PIN));
}

int writePin(int PIN, uint8_t BOOL)
{
	bcm2835_gpio_write(PIN, BOOL);
}

int blink(int PIN, uint64_t delay)
{
     while (1)
     {
          // Turn it on
          bcm2835_gpio_write(PIN, HIGH);

          // wait a bit
          bcm2835_delayMicroseconds(delay);

          // turn it off
          bcm2835_gpio_write(PIN, LOW);

          // wait a bit
          bcm2835_delayMicroseconds(delay);
     }
}

int end()
{
     bcm2835_close();
     return 0;
}

