#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "movements.h"
#include "statusMsg.h"

int main(int argc, char **argv)
{
     // If you call this, it will not actually access the GPIO
     // Use for testing
     //    bcm2835_set_debug(1);
     // Blink
     msgOk("\t>starting init()\n");
     if(init())
          return 1;
     msgOk("\t>init() done\n");
     
     msgOk("\t>front()\n"); 
     front();
     sleep(2);
     msgOk("\t>stop()\n"); 
     stop();
     sleep(2);
     
     msgOk("\t>back()\n"); 
     back();
     sleep(2);
     msgOk("\t>stop()\n"); 
     stop();
     sleep(2);

     msgOk("\t>left()\n"); 
     left();
     sleep(4);
     msgOk("\t>stop()\n"); 
     stop();
     sleep(2);
     
     msgOk("\t>right()\n"); 
     right();
     sleep(4);
     msgOk("\t>stop()\n"); 
     stop();

     bcm2835_close();
     return 0;
}
