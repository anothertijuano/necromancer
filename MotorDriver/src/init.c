#include "movements.h"
#include "statusMsg.h"

int main(int argc, char **argv)
{
     msgOk("\t>starting init()\n");
     if(init())
     {
          msgError("\t>init() failed\n");
          return 1;
     }
     else
          msgOk("\t>init() done\n");
     return 0;
}
